#!/bin/sh
# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>

set -eu

DEF_ALPINE_REPO='http://dl-cdn.alpinelinux.org/alpine'
DEF_ALPINE_SUITES='main,community'
DEF_ALPINE_VERSION='latest-stable'
DEF_PACKAGES='busybox'


usage()
{
	echo "Usage: ${0} [OPTIONS]"
	echo 'This script bootstraps Alpine Linux in [BUILD_DIR].'
	echo '    -a  Set the target architecture [TARGET_ARCH]'
	echo '    -b  Build directory (default: "alpine/<ARCH>/rootfs" [BUILD_DIR]'
	echo '    -C  Only use files from cache (e.g. do not update or download) [CACHE_ONLY]'
	echo '    -c  Cache dir to store/use for packages [CACHE_DIR]'
	echo '    -h  Print this usage'
	echo '    -k  Key public key (or keypair concatenated priv and pub) key in PEM format [SSL_PUB_KEY]'
	echo "    -p  Semi-colon separated list, or file single line entries, of packages to install (default: '${DEF_PACKAGES}') [PACKAGES]"
	echo '    -r  Semi-colon or comma separated list, or file single line entries, of extra repositories to use [REPOSITORIES]'
	echo "    -s  Semi-colon or comma separated list of upstream alpine suites (ex main,community) default: '${DEF_ALPINE_SUITES}' [ALPINE_SUITES]"
	echo '    -u  Allow untrusted packages to be installed. WARNING: This prevents signature validation. [UNTRUSTED]'
	echo
	echo 'The main alpine repository host can be overridden using the [ALPINE_REPO] environment variable.'
	echo
	echo 'All options can also be passed in environment variables (listed between [BRACKETS]).'
	echo 'NOTE: This script requires administrative privileges to run.'
}

_msg()
{
	_level="${1:?Missing argument to function}"
	shift

	if [ "${#}" -le 0 ]; then
		echo "${_level}: No content for this message ..."
		return
	fi

	echo "${_level}: ${*}"
}

e_err()
{
	_msg 'err' "${*}" >&2
}

e_warn()
{
	_msg 'warning' "${*}"
}

e_notice()
{
	_msg 'notice' "${*}"
}

init()
{
	trap cleanup EXIT HUP INT QUIT ABRT ALRM TERM

	if [ -z "${arch:-}" ]; then
		e_err 'Missing architecture, please specify a target architecture [TARGET_ARCH]'
		exit 1
	fi

	if [ -d "${build_dir}" ]; then
		rm -rf "${build_dir:?}/"
	fi
	mkdir -p "${build_dir}"

	if [ -d "${cache_dir:-}" ]; then
		echo "Reusing existing local cache '${cache_dir}'."
	elif [ -n "${cache_dir:-}" ]; then
		mkdir -p "${cache_dir}"
	fi

	for _ssl_pub_key in $(echo "${ssl_pub_keys:-}" | tr -s ' ;\f\n\t\v' ' '); do
		if [ -n "${_ssl_pub_key}" ] && \
		   [ ! -f "${_ssl_pub_key}" ]; then
			_ssl_key_tmp="$(mktemp -p "${TMPDIR:-/tmp}" "ssl_key_tmp-XXXXXXXX")"
			touch "${_ssl_key_tmp}.pub"

			printf "%s" "${_ssl_pub_key%%-----END PRIVATE KEY-----*}-----END PRIVATE KEY-----" | \
			fold > "${_ssl_key_tmp}"
			printf "%s" "${_ssl_pub_key#*-----END PRIVATE KEY-----}" | \
			sed '/./,$!d' | fold > "${_ssl_key_tmp}"

			_ssl_pub_key="${_ssl_key_tmp}"

			e_warn "Installing '${_ssl_pub_key}' into '/etc/apk/keys' for verification."
			install -b -D -m 0644 -t "/etc/apk/keys/" "${_ssl_pub_key}.pub"
			remind_key_cleanup="'/etc/apk/keys/${_ssl_pub_key##*/}.pub'"
		fi
	done
}

cleanup()
{
	if [ -n "${remind_key_cleanup:-}" ]; then
		echo 'Generated or installed keys, remember to clean or store them:'
		echo "${remind_key_cleanup}" | tr -s ' ' '\n'
		echo '-----------------------------------------------------------------'
	fi

	trap - EXIT HUP INT QUIT ABRT ALRM TERM
}

## bootstrap_rootfs() - Generate a new Alpine based rootfs in `-b`
# Optionally takes a key supplied via `-k` and installs it into
# `/etc/apk/keys` to verify against.
# Repositories can be added via either `-r` or `ALPINE_REPOS`, where the later
# puts the content into `/etc/apk/repositories`. If `-r` is prefixed with a
# comma `,`, then The 'host' repositories are expanded with the supplied
# repositories. The List of packages comes from `-p`.
bootstrap_rootfs()
{
	echo "Bootstrapping Alpine Linux rootfs in to '${build_dir}'."

	install -D -d -m 0555 \
	        "${build_dir}/dev" \
	        "${build_dir}/proc" \
	        "${build_dir}/run" \
	        "${build_dir}/sys"

	mknod -m 0600 "${build_dir}/dev/console" c 5 1
	mknod -m 0666 "${build_dir}/dev/null" c 1 3

	install -D -d -m 0755 \
	        "${build_dir}/var/empty"

	install -D -d -m 1777 "${build_dir}/run/lock"

	ln -s '/run' "${build_dir}/var/run"
	ln -s '/run/lock' "${build_dir}/var/lock"

	install -D -d -m 0755 "${build_dir}/etc/apk/keys/"

	for _ssl_pub_key in $(echo "${ssl_pub_keys:-}" | tr -s ' ;\f\n\t\v' ' '); do
		if [ -f "${_ssl_pub_key%*.pub}.pub" ]; then
			install -b -D -m 0644 \
				-t "${build_dir}/etc/apk/keys/" \
				"${_ssl_pub_key%*.pub}.pub"
		fi
	done

	if [ -n "${repositories}" ] && \
	   [ "${repositories#,}" = "${repositories}" ]; then
		echo "# List of repositories manually given at build time" > "${build_dir}/etc/apk/repositories"
		_repositories='--repositories-file /dev/null'
	else
		if [ -n "${ALPINE_REPOS:-}" ]; then
			echo "${ALPINE_REPOS}" | \
			tr -s ' ,;\f\n\t\v' '\n' > "${build_dir}/etc/apk/repositories"
		else
			for _suite in $(echo "${alpine_suites}" | \
			                tr -s ' ,;\f\n\t\v' ' '); do
				echo "${ALPINE_REPO:-${DEF_ALPINE_REPO}}/${ALPINE_VERSION:-${DEF_ALPINE_VERSION}}/${_suite}" >> "${build_dir}/etc/apk/repositories"
			done
		fi
	fi
	for _repository in $(echo "${repositories:-}" | \
	                     tr -s ' ,;\f\n\t\v' ' '); do
		if [ -z "${_repository}" ]; then
			continue
		fi

		if [ -f "${_repository}" ]; then
			__repositories="$(sed 's|#.*||g' "${_repository}") ${__repositories:-}"
		else
			__repositories="${_repository} ${__repositories:-}"
		fi
	done
	for _repository in $(echo "${__repositories:-}" | \
	                     tr -s ' ,;\f\n\t\v' ' '); do
		_repositories="--repository ${_repository} ${_repositories:-}"
	done

	echo 'Adding Alpine-keys'
	# Chicken and egg, we want to install all packages using keys, but we can't
	# install the alpine keys without the alpine keys ... so the keys need to
	# copied from our local installation, as those are the only/most trusted.
	# The keys will be properly reinstalled in the next step.
	case "${arch}" in
	'aarch64' | 'arm'*)
		install -m 0644 \
			'/usr/share/apk/keys/arm'*'/'*'.rsa.pub' \
			'/usr/share/apk/keys/aarch64/'*'.rsa.pub' \
			-t "${build_dir}/etc/apk/keys/"
	;;
	'ppc'*)
		install -D -m 0644 \
			'/usr/share/apk/keys/ppc'*'/'*'.rsa.pub' \
			-t "${build_dir}/etc/apk/keys/"
	;;
	's390x')
		install -D -m 0644 \
			'/usr/share/apk/keys/s390x/'*'.rsa.pub' \
			-t "${build_dir}/etc/apk/keys/"
	;;
	'x86'*)
		install -D -m 0644 \
			'/usr/share/apk/keys/x86'*'/'*'.rsa.pub' \
			-t "${build_dir}/etc/apk/keys/"
	;;
	*)
		e_err "Unsupported architecture '${arch}' selected"
		exit 1
		;;
	esac

	if ! grep -q 'root' "${build_dir}/etc/group" 2> '/dev/null'; then
		e_warn 'No user "root" in "/etc/group"'
		echo 'root:x:0:root' >> "${build_dir}/etc/group"
	fi
	if ! grep -q 'root' "${build_dir}/etc/passwd" 2> '/dev/null'; then
		e_warn 'No user "root" in "/etc/passwd"'
		echo 'root:x:0:0:root:/root:/bin/sh' >> "${build_dir}/etc/passwd"
	fi

	mkdir -p "${build_dir}/usr/bin/"

	echo 'Installing requested packages.'
	# shellcheck disable=SC2046  # allow word splitting for ${packages}
	apk add \
	    ${untrusted:+--allow-untrusted} \
	    --arch "${arch}" \
	    --initdb \
	    --root "${build_dir}" \
	    --update-cache \
	    ${cache_dir:+--cache-dir=${cache_dir}} \
	    ${cache_only:+--no-network} \
	    ${_repositories:+${_repositories}} \
	    alpine-keys $(echo "${packages}" | tr -s ' ;\f\n\t\v' ' ' || true)

	echo 'Adding baselayout'
	apk fetch \
	    --allow-untrusted \
	    --arch "${arch}" \
	    --root "${build_dir}" \
	    ${cache_dir:+--cache-dir=${cache_dir}} \
	    ${cache_only:+--no-network} \
	    ${_repositories:+${_repositories}} \
	    --stdout \
	    alpine-release | tar -xvz -C "${build_dir}" 'etc'

	rm -f "${build_dir}/var/cache/apk/"*
}

main()
{
	_start_time="$(date '+%s')"

	while getopts ':a:b:Cc:hk:n:p:r:s:u' _options; do
		case "${_options}" in
		'a')
			arch="${OPTARG}"
			;;
		'b')
			build_dir="${OPTARG}"
			;;
		'C')
			cache_only='true'
			;;
		'c')
			cache_dir="${OPTARG}"
			;;
		'h')
			usage
			exit 0
			;;
		'k')
			ssl_pub_keys="${OPTARG};${ssl_pub_keys:-}"
			;;
		'p')
			packages="${OPTARG};${packages:-}"
			;;
		'r')
			repositories="${OPTARG};${repositories:-}"
			;;
		's')
			alpine_suites="${OPTARG};${alpine_suites:-}"
			;;
		'u')
			untrusted='true'
			e_warn 'No APK signatures will be checked, this is for development purposes only!'
			;;
		':')
			e_er "Option -${OPTARG} requires an argument."
			exit 1
			;;
		*)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	alpine_suites="${alpine_suites:-${ALPINE_SUITES:-${DEF_ALPINE_SUITES}}}"
	arch="${arch:-${TARGET_ARCH:-}}"
	build_dir="${build_dir:-${BUILD_DIR:-$(pwd)/${BULD_DIR_TEMPLATE:-alpine/${arch}/rootfs}}}"
	cache_dir="${cache_dir:-${CACHE_DIR:-}}"
	cache_only="${cache_only:-${CACHE_ONLY:-}}"
	packages="${packages:-${PACKAGES:-}}"
	repositories="${repositories:-${REPOSITORIES:-}}"
	ssl_pub_keys="${ssl_pub_keys:-${SSL_PUB_KEYS:-}}"
	untrusted="${untrusted:-${UNTRUSTED:-}}"

	if [ -f "${packages}" ]; then
		packages="$(sed 's|#.*||g' "${packages}")"
	fi

	if [ "$(id -u || true)" -ne 0 ]; then
		e_err 'This script requires administrative privileges.'
		e_err "Run this script again with for example 'sudo ${0}'."
		e_err "See ${0} -h for more info."
		exit 1
	fi

	init
	bootstrap_rootfs

	echo "Completed bootstrapping Alpine Linux rootfs in $(($(date '+%s' || true) - _start_time)) seconds."
}

main "${@}"

exit 0
