#!/bin/sh
# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
# Copyright (C) 2020 Thibault Ferrante <thibault.ferrante@pm.me>
# Copyright (C) 2020 EVBox Intelligence B.V.

set -eu

DEF_FORCE_OUTPUT_OVERWRITE='false'
DEF_IMAGE_MULTI_SIGN='false'


REQUIRED_COMMANDS='
	[
	command
	echo
	exit
	getopts
	mktemp
	openssl
	printf
	set
	shift
	tail
	test
	trap
	unlink
'


usage()
{
	echo "Usage: ${0} [OPTIONS] -c <cert_chain> -k <private_key> <image_file>"
	echo
	echo 'Sign the update and embed the certificate chain into the file'
	echo '    -c  Certificate chain to embed'
	echo "    -f  Force overwriting output file (default: ${DEF_FORCE_OUTPUT_OVERWRITE})[FORCE_OUTPUT_OVERWRITE]"
	echo '    -h  Print this help text and exit'
	echo '    -k  Private key to sign update with'
	echo "    -m  Multi-sign image (default: ${DEF_IMAGE_MULTI_SIGN}) [IMAGE_MULTI_SIGN]"
	echo "    -o  Output signed image file, by default \${image_file}.sign [OUTPUT_SIGNED_IMAGE]"
	echo '    -p  Print used certificate information verbosely'
	echo
	echo 'image_file to append the signature and certificates to'
	echo
	echo 'All options can also be passed in environment variables (listed between [BRACKETS]).'
	echo 'NOTE: This script requires administrative privileges to run.'

}

_msg()
{
	_level="${1:?Missing argument to function}"
	shift

	if [ "${#}" -le 0 ]; then
		echo "${_level}: No content for this message ..."
		return
	fi

	echo "${_level}: ${*}"
}

e_err()
{
	_msg 'err' "${*}" >&2
}

e_warn()
{
	_msg 'warning' "${*}"
}

e_notice()
{
	_msg 'notice' "${*}"
}

cleanup()
{
	if [ -f "${leaf_certificate:-}" ]; then
		unlink "${leaf_certificate}"
	fi

	if [ -f "${image_signature:-}" ]; then
		unlink "${image_signature}"
	fi

	if [ -f "${leaf_public_key:-}" ]; then
		unlink "${leaf_public_key}"
	fi

	if [ -f "${public_key:-}" ]; then
		unlink "${public_key}"
	fi

	if [ -f "${output_tmp}" ]; then
		unlink "${output_tmp}"
	fi

	trap - EXIT HUP INT QUIT ABRT ALRM TERM
}

init()
{
	trap cleanup EXIT HUP INT QUIT ABRT ALRM TERM

	if [ ! -f "${cert_chain:-}" ]; then
		e_err "Unable to access '${cert_chain:-cert_chain}', cannot continue"
		exit 1
	fi

	if [ ! -f "${private_key:-}" ]; then
		e_err "Unable to access '${private_key:-private_key}', cannot continue"
		exit 1
	fi

	if [ -f "${output_file:-}" ]; then
		e_warn "Output file '${output_file}' already exists"
		if [ "${force_output_overwrite:-}" = 'true' ]; then
			unlink "${output_file}"
		else
			e_err "Refusing to continue, use '-f' to force overwrite"
			exit 1
		fi
	elif [ -d "${output_file:-}" ]; then
		output_file="${output_file}/${image_file:-IMAGE_FILE}.sign"
	fi

	output_tmp="$(mktemp -p "$(dirname "${output_file:-OUTPUT_SIGNED_IMAGE}")" 'output_tmp.XXXXXX')"
	leaf_public_key="$(mktemp -p "${TMPDIR:-/tmp}" 'leaf_public_key.pem.XXXXXX')"
	public_key="$(mktemp -p "${TMPDIR:-/tmp}" 'public_key.pem.XXXXXX')"
	image_signature="$(mktemp -p "${TMPDIR:-/tmp}" 'image_signature.sig.XXXXXX')"

	cp "${image_file}" "${output_tmp}"
}

check_requirements()
{
	for _cmd in ${REQUIRED_COMMANDS}; do
		if ! _test_result="$(command -V "${_cmd}")"; then
			_test_result_fail="${_test_result_fail:-}${_test_result}\n"
		else
			_test_result_pass="${_test_result_pass:-}${_test_result}\n"
		fi
	done

	if [ -n "${_test_result_fail:-}" ]; then
		e_err 'Self-test failed, missing dependencies.'
		echo '======================================='
		echo 'Passed tests:'
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_pass:-none\n}"
		echo '---------------------------------------'
		echo 'Failed tests:'
		# shellcheck disable=SC2059  # Interpret \n from variable
		printf "${_test_result_fail:-none\n}"
		echo '======================================='
		exit 1
	fi
}

append_marker()
{
	_input_file="${1?}"

	if [ "${MULTI_SIGN_IMAGE:-}" != 'false' ]; then
		return
	fi

	printf '%08x' 0 | xxd -r -p >> "${_input_file}"
}

append_metadata()
{
	_input_file="${1?}"
	_metadata="${2?}"

	cat "${_metadata}" >> "${_input_file}"

	# Workaround as busybox `tac` cannot do --regex --separator
	# see https://git.busybox.net/busybox/tree/coreutils/tac.c#n49
	__metadata_hex="$(printf '%08x' "$(stat -c '%s' "${_metadata}" || true)")"
	_metadata_hex="$(echo "${__metadata_hex}" | cut -c 7-8)"
	_metadata_hex="${_metadata_hex}$(echo "${__metadata_hex}" | cut -c 5-6)"
	_metadata_hex="${_metadata_hex}$(echo "${__metadata_hex}" | cut -c 3-4)"
	_metadata_hex="${_metadata_hex}$(echo "${__metadata_hex}" | cut -c 1-2)"
	printf '%s' "${_metadata_hex}" | xxd -r -p >> "${_input_file}"

}

sign_file()
{
	_private_key="${1?}"
	_sign_file="${2?}"

	if ! openssl dgst \
	             -sha512 \
	             -sign "${_private_key}" \
	             -out "${image_signature}" \
	             "${_sign_file}"; then
		e_err "Unable to sign file '${_sign_file}'"
		exit 1
	fi
}

check_key()
{
	if [ -n "${verbose:-}" ]; then
		openssl x509 -text -in "${cert_chain}"
	fi
	openssl x509 -pubkey -noout -in "${cert_chain}" > "${leaf_public_key}"
	openssl rsa -in "${private_key}" -pubout -out "${public_key}"

	if ! cmp -s "${leaf_public_key}" "${public_key}"; then
		e_err 'Certificate and key do not match'
		exit 1
	fi
}

main()
{
	while getopts ':hc:fk:mo:p' _options; do
		case "${_options}" in
		'c')
			cert_chain="${OPTARG}"
			;;
		'f')
			force_output_overwrite='true'
			;;
		'h')
			usage
			exit 0
			;;
		'k')
			private_key="${OPTARG}"
			;;
		'm')
			image_multi_sign='true'
			;;
		'o')
			output_file="${OPTARG}"
			;;
		'p')
			verbose='true'
			;;
		':')
			e_err "Option -${OPTARG} requires an argument"
			usage
			exit 1
			;;
		*)
			e_err "Invalid option: -${OPTARG}"
			usage
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"
	if [ "${#}" -lt 1 ]; then
		e_err 'Missing argument'
		usage
		exit 1
	fi

	if [ "${#}" -gt 1 ]; then
		e_err 'Too many arguments'
		usage
		exit 1
	fi

	image_file="${1:-}"
	if [ ! -f "${image_file}" ]; then
		e_err 'Missing file to sign'
		exit 1
	fi

	force_output_overwrite="${force_output_overwrite:-${FORCE_OUTPUT_OVERWRITE:-${DEF_FORCE_OUTPUT_OVERWRITE}}}"
	image_multi_sign="${image_multi_sign:-${IMAGE_MULTI_SIGN:-${DEF_IMAGE_MULTI_SIGN}}}"
	output_file="${output_file:-${OUTPUT_SIGNED_IMAGE:-${image_file}.sign}}"

	check_requirements
	init

	check_key
	append_marker "${output_tmp}"
	append_metadata "${output_tmp}" "${cert_chain}"
	sign_file "${private_key}" "${output_tmp}"
	append_metadata "${output_tmp}" "${image_signature}"
	cp "${output_tmp}" "${output_file}"

	cleanup
}

main "${@}"

exit 0
