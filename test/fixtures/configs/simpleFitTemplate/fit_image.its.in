/* SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * Copyright (C) 2018 Olliver Schinagl <oliver@schinagl.nl>
 */

/dts-v1/;

/ {
	description = "Image config for @BOARD_NAME@ - Version: @VERSION@ (@BUILD_ID@)";
	#address-cells = <1>;

	images {
		kernel-1 {
			description = "Linux kernel";
			data = /incbin/("@DATA_KERNEL@");
			type = "kernel";
			arch = "@ARCH@";
			os = "linux";
			compression = "@COMPRESSION_TYPE@";
			hash-1 {
				algo = "sha256";
				uboot-ignore = <1>;
			};
		};

		fdt-1 {
			description = "Flattened device tree blob";
			data = /incbin/("@DATA_DEVICETREE@");
			type = "flat_dt";
			arch = "@ARCH@";
			os = "linux";
			compression = "@COMPRESSION_TYPE@";
			hash-1 {
				algo = "sha256";
				uboot-ignore = <1>;
			};
		};
	};

	configurations {
		default = "main-1";

		main-1 {
			description = "Main boot configuration";
			kernel = "kernel-1";
			fdt = "fdt-1";
			signature-1 {
				algo = "sha256,rsa4096";
				key-name-hint = "@KEY_NAME_HINT@";
				sign-images = "fdt", "kernel";
			};
		};
	};
};
