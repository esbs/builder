#!/bin/sh
# SPDX-License-Identifier: AGPL-1.0-or-later
#
# Copyright (C) 2021 Olliver Schinagl <oliver@schinagl.nl>

set -eu


usage()
{
	echo "Usage: ${0} [OPTIONS]"
	echo 'Run this repositories tests.'
	echo '    -h   Print usage'
}

_msg()
{
	_level="${1:?Missing argument to function}"
	shift

	if [ "${#}" -le 0 ]; then
		echo "${_level}: No content for this message ..."
		return
	fi

	echo "${_level}: ${*}"
}

e_err()
{
	_msg 'err' "${*}" >&2
}

e_warn()
{
	_msg 'warning' "${*}"
}

e_notice()
{
	_msg 'notice' "${*}"
}

run_tests()
{
	if [ "${#}" -ge 1 ]; then
		for _test_script in "${@}"; do
			echo "Running shunit2 on '${_test_script}'"
			shunit2 "${_test_script}"
		done
	else
		find 'test/' \
			-path '*fixtures*' -prune -o \
			-type f -perm '/a+x' -iname '*.sh' | while read -r _test_script; do
			if [ ! -f "${_test_script}" ]; then
			     continue
			fi
			echo "Running shunit2 on '${_test_script}'"
			shunit2 "${_test_script}"
		done
	fi
}

main()
{
	_start_time="$(date '+%s')"

	while getopts ':h' _options; do
		case "${_options}" in
		'h')
			usage
			exit 0
			;;
		':')
			e_err "Option -${OPTARG} requires an argument."
			exit 1
			;;
		*)
			e_err "Invalid option: -${OPTARG}"
			exit 1
			;;
		esac
	done
	shift "$((OPTIND - 1))"

	./buildenv_check.sh
	run_tests "${@}"

	echo "==============================================================================="
	echo "Ran tests in $(($(date '+%s' || true) - _start_time)) seconds"
	echo "==============================================================================="
}

main "${@}"

exit 0
